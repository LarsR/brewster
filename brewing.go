package main

import (
	"encoding/json"
	"time"
)

type Brewing struct {
	Uid         int       `json:"uid"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Scheme      string    `json:"scheme"`
	Ingredients string    `json:"ingredients"`
	OG          float64   `json:"og"`
	FG          float64   `json:"fg"`
	IBU         float64   `json:"ibu"`
	BrewingDay  string    `json:"brewingday"`
	Created     time.Time `json:"created"`
	Updated     time.Time `json:"updated"`
}

func (b *Brewing) Save() error {
	return database.AddBrewing(b)
}

func (b *Brewing) Update() error {
	return database.UpdateBrewing(b)
}

func (b *Brewing) Json() ([]byte, error) {
	return json.Marshal(b)
}
