DROP TABLE IF EXISTS `brewings`;
CREATE TABLE `brewings` (
  `uid` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT '',
  `scheme` text NOT NULL,
  `ingredients` text NOT NULL,
  `og` float DEFAULT '0',
  `fg` float DEFAULT '0',
  `ibu` float DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
LOCK TABLES `brewings` WRITE;
INSERT INTO `brewings` VALUES (1,'Bryggning 2017-06-09','En lätt humlad lager med en torrhumling av Amarillo.\n','13:30 - 21l vatten värms 67°C\n14:20 - Mäskning börjar\n15:15 - Lakar 14l\n16:15 - kok start\n16:45 - 33g Saaz (pellets) 60m\n17:15 -  33g Saaz (pellets) 30m\n17:30 - 33g Saaz (pellets) 8m\n17:38 - Börjar kyla\n\n3/7 tillsatte 20g Amarillo pellets.\n13/7 temp 18°C\n17/7 temp 1°C\n18/7 Blanka \n22/7 Tappa upp på fat','6kg Pale Ale Malt\n33g Saaz (pellets) 60m\n33g Saaz (pellets) 30m\n33g Saaz (pellets) 15m\n20g Amarillo (pellets) 0m\n20g M76 Bavarian Lager jäst',1.062,1.014,20,'2017-09-12 19:24:39','2017-09-12 19:29:57'),(2,'Bryggning 2017-08-06','En väldigt naken lager ','10:20 - 22l vatten värms 64°C\n11:10 - Mäskning börjar\n12:25 - Lakar 12l\n13:10 - kok start\n13:40 - 20g okänd humle\n14:20 - 20g Hallertauer 20m\n14:30 - 20g Hallertauer 10m\n14:40 - Kylning\n\n6/8 - 13°C\n13/8 - 16°C\n17/8 - 0.5°C\n28/8 - 3.5°C\n1/9 -0.5 + Blanka\n3/9 Tappa upp på fat\n','6kg Pale Ale Malt\n20g okänd 6-10% 60min\n20g Hallertauer Mittelfrüh (pellets) 20m\n10g Hallertauer Mittelfrüh (pellets) 20m\n2*11.5g W-34/70\n',1.05,1.01,20,'2017-09-12 19:41:29','2017-09-12 19:41:29'),(3,'Bryggning 2017-09-10','Engelsk Ale med inte så mycket humle','7:30 - 22l vatten värms 65°C\n8:10 - Mäskning börjar\n9:10 - Lakar 10l\n9:50 - kok start\n9:50 - 20g okänd humle\n10:20 - 10g Kent Golding\n10:40 - 10g Kent Golding\n10:50 - Kylning','5.5kg Pale Ale Malt\n20g okänd 6-10% 60min\n10g east Kent Golding (kottar) 5.9% 30m\n10g east Kent Golding (kottar) 5.9% 10m\nSafale us-05 11.5g (jäst)',1.052,1.012,20,'2017-09-12 19:46:56','2017-09-12 19:56:07');
UNLOCK TABLES;
