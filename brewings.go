package main

import (
	"encoding/json"
)

type Brewings []*Brewing

func (bs *Brewings) Add(b *Brewing) {
	*bs = append(*bs, b)
}

func (bs *Brewings) Find(uid int) *Brewing {
	for _, d := range *bs {
		if d.Uid == uid {
			return d
		}
	}
	return nil

}
func (bs *Brewings) Json() ([]byte, error) {
	return json.Marshal(bs)
}
func (bs *Brewings) Load() {

}
