package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/rehn/gpio"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
)

type Thermostats []*Thermostat

func (thermostats *Thermostats) Read() {

	for _, t := range *thermostats {
		changed := false
		if !*t.On {
			continue
		}
		device := "/sys/bus/w1/devices/" + t.Uid + "/w1_slave"
		if Mock {
			device = "w1_slave"
		}
		data, err := ioutil.ReadFile(device)
		if err != nil {
			fmt.Println(err)
			return
		}
		str := string(data)
		r, err := regexp.Compile("t=([- 0-9]+)")
		if err != nil {
			fmt.Println(err)
			return
		}

		s := r.FindString(str)[2:]
		num, err := strconv.Atoi(s)
		if err != nil {
			fmt.Println(err)
			return
		}
		ct := float64(float64(num) / 1000)
		if ct != *t.CurrentTemperature {
			changed = true
		}
		*t.CurrentTemperature = ct
		if *t.On {
			if *t.Cool == true {
				if *t.CurrentTemperature > *t.TargetTemperature {
					*t.Active = true
					t.gg.SetLow()

				}
				if *t.CurrentTemperature <= *t.TargetTemperature {
					*t.Active = false
					t.gg.SetHigh()
				}
			} else {
				if *t.CurrentTemperature >= *t.TargetTemperature {
					*t.Active = false
					t.gg.SetHigh()
				}

				if *t.CurrentTemperature < *t.TargetTemperature {
					*t.Active = true
					t.gg.SetLow()
				}
			}
			if changed {
				database.LogStatistics(t.Id, *t.Active, *t.CurrentTemperature)
			}
		}
	}
}

func (thermostats *Thermostats) Get(id int) *Thermostat {
	for _, t := range *thermostats {
		if t.Id == id {
			return t
		}
	}
	return nil
}

func (t *Thermostats) Set(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var new_t Thermostat
	err := decoder.Decode(&new_t)
	if err != nil {
		fmt.Fprint(w, err.Error())
		return
	}

	org := t.Get(new_t.Id)
	if org == nil {
		fmt.Fprint(w, errors.New("Can not find Thermostat with id "+string(new_t.Id)))
		return
	}
	if *org.On != *new_t.On && !*new_t.On {
		*org.Active = false
	}

	*org.On = *new_t.On
	*org.Cool = *new_t.Cool

	*org.TargetTemperature = *new_t.TargetTemperature
	var b []byte
	b, err = json.Marshal(org)
	if err != nil {
		fmt.Fprint(w, err.Error())
		return
	}
	config.Save()

	fmt.Fprint(w, string(b))

}

func (t *Thermostats) Json(w http.ResponseWriter, r *http.Request) {
	b, err := json.Marshal(t)
	if err != nil {
		fmt.Fprint(w, err.Error())
		return
	}
	fmt.Fprint(w, string(b))
}

type Thermostat struct {
	Uid                string   `json:"uid"`
	Id                 int      `json:"id"`
	CurrentTemperature *float64 `json:"currentTemperature"`
	TargetTemperature  *float64 `json:"targetTemperature"`
	Active             *bool    `json:"active"`
	On                 *bool    `json:"on"`
	Cool               *bool    `json:"cool"`
	Gpio               *int     `json:"gpio"`
	gg                 gpio.Gpio
}
