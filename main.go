package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"
)

const (
	DATE_FORMAT       = "2006-01-02 15:04:05"
	SHORT_DATE_FORMAT = "2006-01-02"
)

var (
	httpd      Httpd
	database   DB
	brewings   Brewings
	config     Config
	configFile string
	Mock       bool
)

func main() {
	flag.BoolVar(&Mock, "mock", false, "use mock instead of real sensors")
	flag.StringVar(&configFile, "config", "assets/config.json", "Configurationfile to use")
	migrate := flag.String("migrate", "", "use file to do a miration of database")

	flag.Parse()

	err := config.LoadFromJsonFile()
	if err != nil {
		log.Print(err)
	}

	if *migrate != "" {

		file, err := PathToABS(*migrate)
		if err != nil {
			log.Print(err)
		} else {

			err = database.Migrate(file)
			if err != nil {
				log.Print(err)
			}
		}
		return
	}

	brewings, err = database.getBrewings()
	if err != nil {
		log.Print(err)
	}
	go func() {
		httpd.StartServer()
	}()

	go func() {
		for {
			select {
			case <-time.After(5 * time.Second):
				config.Thermostats.Read()
			}
		}

	}()

	sigterm := make(chan os.Signal, 1)
	signal.Notify(sigterm, syscall.SIGHUP, os.Kill, os.Interrupt, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGKILL)
	<-sigterm
	httpd.dumpSessionsToDisk()
}

func PathToABS(inPath string) (string, error) {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return inPath, err
	}
	if !strings.HasPrefix(inPath, "/") {
		inPath = dir + "/" + inPath
	}
	return inPath, nil
}
