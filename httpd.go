package main

import (
	"encoding/json"
	"fmt"
	"github.com/satori/go.uuid"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// Httpd Container for httpd server
type Httpd struct {
	server   string
	Sessions []*Session
}

var (
	timeout time.Duration = time.Hour
)

type Session struct {
	Guid    string
	Timeout *time.Time
}

// StartServer formats using the default formats for its operands and writes to w.
func (h *Httpd) StartServer() {

	http.Handle("/", http.HandlerFunc(h.getFile))
	http.Handle("/brewings", http.HandlerFunc(h.brewings))
	http.Handle("/sensors", http.HandlerFunc(config.Thermostats.Json))
	http.Handle("/ontap", http.HandlerFunc(h.onTap))
	http.Handle("/save_tap", http.HandlerFunc(h.saveTap))
	http.Handle("/save_brewing", http.HandlerFunc(h.saveBrewing))
	http.Handle("/get_statistics", http.HandlerFunc(h.Statistics))
	http.Handle("/login", http.HandlerFunc(h.login))
	http.Handle("/logout", http.HandlerFunc(h.logout))
	http.Handle("/loggedin", http.HandlerFunc(h.loggedIn))
	http.Handle("/set_thermostat", http.HandlerFunc(config.Thermostats.Set))
	go func() {
		for {
			select {
			case <-time.After(10 * time.Second):
				h.removeSession()
			}
		}
	}()
	h.initSessionsFromDisk()
	http.ListenAndServe(":8880", nil)
}

func (h *Httpd) brewings(w http.ResponseWriter, r *http.Request) {
	b, err := brewings.Json()
	if err != nil {
		fmt.Fprint(w, err.Error())
		return
	}
	fmt.Fprint(w, string(b))
}

func (h *Httpd) Statistics(w http.ResponseWriter, r *http.Request) {
	i := r.FormValue("id")
	f := r.FormValue("from")
	t := r.FormValue("to")
	if i == "" {
		h.badRequest(w)
		return
	}
	id, err := strconv.Atoi(i)
	if err != nil {
		log.Print(err)
		h.internalError(w, err.Error())
		return
	}

	from, err := time.Parse(DATE_FORMAT, f)
	if err != nil {
		from = time.Now().Add(-3 * time.Hour)
	}

	to, err := time.Parse(DATE_FORMAT, t)
	if err != nil {
		to = time.Now()
	}

	st, err := database.GetStatistics(id, from, to)
	if err != nil {
		log.Print(err)
		h.internalError(w, err.Error())
		return
	}

	b, err := json.Marshal(st)
	if err != nil {
		log.Print(err)
		h.internalError(w, err.Error())
		return
	}
	fmt.Fprint(w, string(b))
}

func (h *Httpd) onTap(w http.ResponseWriter, r *http.Request) {
	b, err := json.Marshal(config.Taps)
	log.Println(config)
	if err != nil {
		h.internalError(w, err.Error())
		return
	}
	fmt.Fprint(w, string(b))
}

func (h *Httpd) saveTap(w http.ResponseWriter, r *http.Request) {
	if !h.haveSession(r) {
		h.unauthorized(w)
		return
	}
	name := r.FormValue("name")
	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		h.internalError(w, err.Error())
		return
	}
	if name == "" {
		h.badRequest(w)
		return
	}

	for _, t := range config.Taps {
		if t.Name == name {
			*t.Id = id
			err = config.Save()
			if err != nil {
				h.internalError(w, err.Error())
				return
			}
		}
	}
}

func (h *Httpd) logout(w http.ResponseWriter, r *http.Request) {
	if h.haveSession(r) {
		c, _ := r.Cookie("SESSION")
		h.removeSessionCookie(c, w)
		h.removeSession()
	}
	b, _ := json.Marshal(false)
	fmt.Fprint(w, string(b))
}

func (h *Httpd) loggedIn(w http.ResponseWriter, r *http.Request) {
	res := h.haveSession(r)
	b, _ := json.Marshal(res)
	fmt.Fprint(w, string(b))

}

func (h *Httpd) login(w http.ResponseWriter, r *http.Request) {
	if h.haveSession(r) {
		b, _ := json.Marshal(true)
		fmt.Fprint(w, string(b))
		return
	}
	if r.FormValue("password") == config.Admin.Password && r.FormValue("username") == config.Admin.User {
		uid := uuid.NewV1()
		s := new(Session)
		s.Guid = uid.String()
		s.Timeout = new(time.Time)
		*s.Timeout = time.Now().Add(time.Hour * 1)
		h.Sessions = append(h.Sessions, s)
		cookie := &http.Cookie{Name: "SESSION", Expires: *s.Timeout, Value: s.Guid, HttpOnly: false}
		http.SetCookie(w, cookie)
		b, _ := json.Marshal(true)
		fmt.Fprint(w, string(b))
		return
	}

	b, _ := json.Marshal(false)
	fmt.Fprint(w, string(b))
}

func (h *Httpd) haveSession(r *http.Request) bool {
	c, err := r.Cookie("SESSION")
	if err != nil {
		return false
	}

	for _, s := range h.Sessions {
		if s.Guid == c.Value {
			return true
		}
	}
	return false
}

func (h *Httpd) updateSessionTimeout(cookie http.Cookie, w http.ResponseWriter) {
	for _, sess := range h.Sessions {
		if sess.Guid == cookie.Value {
			s := new(Session)
			s.Guid = sess.Guid
			s.Timeout = new(time.Time)
			*s.Timeout = time.Now().Add(time.Hour * 1)
			*sess.Timeout = *s.Timeout
			cookie := &http.Cookie{Name: "SESSION", Expires: *s.Timeout, Value: s.Guid, HttpOnly: false}
			http.SetCookie(w, cookie)
		}
	}
}

func (h *Httpd) removeSession() {
	for i, sess := range h.Sessions {
		if time.Now().After(*sess.Timeout) {
			fmt.Println("Remove session:", sess.Guid)
			h.Sessions = append(h.Sessions[:i], h.Sessions[i+1:]...)
		}
	}
}
func (h *Httpd) removeSessionCookie(cookie *http.Cookie, w http.ResponseWriter) {
	for _, sess := range h.Sessions {
		if sess.Guid == cookie.Value {
			s := new(Session)
			s.Guid = sess.Guid
			s.Timeout = new(time.Time)
			*s.Timeout = time.Now()
			*sess.Timeout = *s.Timeout
			cookie := &http.Cookie{Name: "SESSION", Expires: *s.Timeout, Value: s.Guid, HttpOnly: false}
			http.SetCookie(w, cookie)
		}
	}
}

func (h *Httpd) saveBrewing(w http.ResponseWriter, r *http.Request) {

	if !h.haveSession(r) {
		h.unauthorized(w)
		return
	}

	decoder := json.NewDecoder(r.Body)
	var b Brewing
	err := decoder.Decode(&b)
	if err != nil {
		fmt.Fprint(w, err.Error())
		return
	}
	if b.Uid == 0 {
		err = b.Save()
		brewings.Add(&b)

	} else {
		err = b.Update()
		br := brewings.Find(b.Uid)
		if br != nil {
			*br = b
		}
	}
	if err != nil {
		fmt.Fprint(w, err.Error())
	}
	bb, err := b.Json()
	if err != nil {
		h.internalError(w, err.Error())
		return
	}
	fmt.Fprint(w, string(bb))
}

func (h *Httpd) initSessionsFromDisk() {
	file, err := PathToABS(".sessions")
	if err != nil {
		fmt.Println(err)
		return
	}
	b, err := ioutil.ReadFile(file)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = json.Unmarshal(b, &h.Sessions)
	if err != nil {
		fmt.Println(err)
	}
}

func (h *Httpd) dumpSessionsToDisk() {
	file, err := PathToABS(".sessions")
	if err != nil {
		return
	}

	b, err := json.Marshal(h.Sessions)
	if err != nil {
		return
	}
	err = ioutil.WriteFile(file, b, 0644)
	return
}

func (h *Httpd) getFile(w http.ResponseWriter, r *http.Request) {
	if r.RequestURI == "/" {
		cookie, _ := r.Cookie("SESSION")
		if cookie != nil {
			h.updateSessionTimeout(*cookie, w)
		}
		p, _ := PathToABS("assets/html/index.html")
		http.ServeFile(w, r, p)
	} else {
		p, _ := PathToABS("assets/" + strings.TrimLeft(r.RequestURI, "/"))
		http.ServeFile(w, r, p)
	}
}

func (h Httpd) unauthorized(w http.ResponseWriter) {
	w.WriteHeader(http.StatusUnauthorized)
	w.Write([]byte("401: Unauthorized"))

}

func (h Httpd) badRequest(w http.ResponseWriter) {
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte("400: Bad Request"))
}

func (h Httpd) internalError(w http.ResponseWriter, err string) {
	w.WriteHeader(http.StatusInternalServerError)
	log.Println("500: Internal Server Error (" + err + ")")
	w.Write([]byte("500: Internal Server Error (" + err + ")"))
}
