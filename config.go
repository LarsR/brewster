package main

import (
	"encoding/json"
	"github.com/rehn/gpio"
	"io/ioutil"
)

type Config struct {
	Thermostats *Thermostats
	Mysql       struct {
		User     string `json:"user"`
		Password string `json:"password"`
		Database string `json:"database"`
	} `json:"mysql"`
	Admin struct {
		User     string `json:"user"`
		Password string `json:"password"`
	} `json:"admin"`
	Taps []Tap `json:"taps"`
}
type Tap *struct {
	Id   *int   `json:"id"`
	Name string `json:"name"`
}

func (c *Config) LoadFromJsonFile() error {
	file, err := PathToABS(configFile)
	if err != nil {
		return err
	}
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, c)
	for _, t := range *c.Thermostats {
		t.Active = new(bool)

		t.CurrentTemperature = new(float64)
		if !Mock || *t.Gpio != 0 {
			t.gg = gpio.NewGpio("out", *t.Gpio)
			t.gg.SetHigh()
		}
	}
	return err
}

func (c Config) Save() error {
	file, err := PathToABS(configFile)
	if err != nil {
		return err
	}

	b, err := json.Marshal(c)
	if err != nil {
		return nil
	}
	err = ioutil.WriteFile(file, b, 0644)
	return err
}
