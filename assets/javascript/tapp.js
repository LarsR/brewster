TapPage = function(app) {
  this.count = 3;
  this.tappar = [];
  this.app = app;
  this.render();
}

TapPage.prototype.show = function() {
  this.load();
  dd.dom.removeClass(this.html,'hide');
}

TapPage.prototype.hide = function() {
  dd.dom.removeChildren(this.html);
  dd.dom.addClass(this.html,'hide');
}

TapPage.prototype.update = function() {

}

TapPage.prototype.render = function() {
  this.html = dd.dom.createElement('div',{id: 'ontap', class: 'hide'});
  dd.dom.append(this.html, dd.dom.getElement('main'));
}


TapPage.prototype.load = function(){
  req = new dd.net.XhrIo('/ontap','GET', null,this);
  req.onComplete = function(d){
    dd.array.each(d.responseJson,function(a,b){
      var tap = new Tap(a, this.app)
      dd.dom.insertFirst(tap.html(), this.html);
    },this)
  }
  req.onError = function(e){
    console.log(e);
  }
  req.send();
}

TapPage.prototype.pause = function() {

}

Tap = function(data, app) {
  this.app = app;
  this.evh = new dd.events.EventHandler();
  this.id = 0;
  this.tap = data.name;
  if (data.id != 0) {
    this.brew = this.app.Brewings.getById(data.id);
    this.id = data.id;
  }
  this.app.Login.listen('LOGGED_OUT',this.onLogout,this);
  this.app.Login.listen('LOGGED_IN',this.onLogin,this);
}

Tap.prototype.change = function() {
  this.id = this.select.value;
  this.brew = this.app.Brewings.getById(this.id);
  this.save();
}

Tap.prototype.save = function () {

  req = new dd.net.XhrIo('/save_tap','POST', 'name=' + this.tap + '&id=' + this.id,this);

  req.onComplete = function(e) {
    this.render();
  }
  req.onError = function(e){
    Notifications.varning(e.responseText);
  }
  req.setRequestHeader({'Content-type': 'application/x-www-form-urlencoded'});
  req.send();
}

Tap.prototype.onLogin = function(){
  this.render()
}

Tap.prototype.onLogout = function(){
  this.render()
}

Tap.prototype.render = function() {
  dd.dom.removeChildren(this._html);
  var name = "";
  var description = "Denna tapp saknar öl!";

  if (this.brew){
    name = this.brew.name;
    description = this.brew.description;
  }

  if (this.app.Login.isloggedin) {
    var options = [dd.dom.createElement('option',{value: 0},'---')];
    dd.array.each(this.app.Brewings.items,function(brewing) {
      var attr = {value: brewing.uid}
      if (brewing.uid == this.id){
        attr.selected = "selected";
      }
      var option = dd.dom.createElement('option',attr, brewing.name)
      options.push(option);
    },this);
    this.select= dd.dom.createElement('select',{},options);
    this.evh.listen(this.select,'change',this.change,this);
  }else{
    this.app.Login
    this.select= dd.dom.createElement('span', {},name);
  }
  var desc = dd.dom.createElement('div',{class:'tap-description'});
  desc.insertAdjacentHTML('beforeend', description);
  var html = [
    dd.dom.createElement('div',{},dd.dom.createElement('h2',{},this.select)),
    dd.dom.createElement('div',{class:'tap-name'},this.select),
    desc
  ];
  dd.dom.append(html,this._html);
}

Tap.prototype.html = function() {
  if (this._html){
    dd.dom.remove(this._html);
  }
  this._html = dd.dom.createElement('div',{class: 'tapp tap-'+this.tap});
  this.render();
  return this._html;
}


