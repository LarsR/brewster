ToolsPage = function() {
    this.evh = new dd.events.EventHandler();
    this.og = dd.dom.createElement('input',{class: 'og',type:'number',step:'0.001',value:1.052});
    this.fg = dd.dom.createElement('input',{class: 'fg',type:'number',step:'0.001',value:1.012});
    this.alco = dd.dom.createElement('span',{},'5.25')
    this.evh.listen(this.og,'change',this.calculate,this);
    this.evh.listen(this.fg,'change',this.calculate,this);

    this.render();


}

ToolsPage.prototype.show = function() {
  dd.dom.removeClass(this.html,'hide');
}

ToolsPage.prototype.hide = function() {
  dd.dom.addClass(this.html,'hide');
}

ToolsPage.prototype.update = function() {

}

ToolsPage.prototype.render = function() {
  this.html = dd.dom.createElement('div',{class:'hide'},[
    dd.dom.createElement('div',{id:'calculator'},[
      dd.dom.createElement('table',{width:200},[
        dd.dom.createElement('thead',{},[
          dd.dom.createElement('tr',{},[
            dd.dom.createElement('td',{colspan:3,align:'center'},[
              dd.dom.createElement('h3',{},"Alkohol Kalkylator")
            ])
          ])
        ]),
        dd.dom.createElement('tbody',{},[
          dd.dom.createElement('tr',{},[
            dd.dom.createElement('td',{align: 'center',width:60},'OG'),
            dd.dom.createElement('td',{align: 'center',width:60},'FG'),
            dd.dom.createElement('td',{align: 'center'},'%')
          ]),
          dd.dom.createElement('tr',{},[
            dd.dom.createElement('td',{align: 'center',width:60},this.og),
            dd.dom.createElement('td',{align: 'center',width:60},this.fg),
            dd.dom.createElement('td',{align: 'center'},this.alco)
          ])
        ])
      ])
    ])
  ]);
  dd.dom.append(this.html, dd.dom.getElement('main'));
}

ToolsPage.prototype.calculate = function() {

  vol = parseFloat((this.og.value - this.fg.value) * 131.25).toFixed(2);


  dd.dom.setContent(vol+ "%",this.alco);
}

ToolsPage.prototype.pause = function() {

}
Tools={};

Tools.CalculateAlcohol=function(og,fg) {
return parseFloat((og - fg) * 131.25).toFixed(2);
}