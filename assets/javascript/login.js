Login = function() {
  this.evh = new dd.events.EventHandler();
  this.isloggedin = false;
  this.loggedin();
}
dd.extend(Login, dd.events.EventTarget);

Login.prototype.render = function() {
  this.username = dd.dom.createElement('input', {type:'text',placeholder:'username'});
  this.password = dd.dom.createElement('input', {type:'password',placeholder:'password'});
  this.loginBtn = dd.dom.createElement('button', {id:'login-btn'});

  this.loginDom = dd.dom.createElement('div',{class: 'login'},[
    this.username,
    this.password,
    this.loginBtn
  ]);

  this.logoutBtn = dd.dom.createElement('button', {id:'logout-btn'});
  this.logoutDom = dd.dom.createElement('div',{class: 'logout hide'},[
    dd.dom.createElement('span',{class:'login-username'},'Admin'),
    this.logoutBtn
  ]);

  this.html = dd.dom.createElement('div',{class: 'login-base'},[
    this.loginDom,
    this.logoutDom
  ]);
  this.evh.listen(this.password,'keypress',this.doKeyPress,this);
  this.evh.listen(this.loginBtn,'click',this.doLogin,this);
  this.evh.listen(this.logoutBtn,'click',this.doLogout,this);
  return this.html;

}


Login.prototype.loggedin = function() {
  var req = new dd.net.XhrIo('/loggedin','GET',null,this);
  req.onComplete = function(e) {
    this.updateGui(e.responseJson);
    var that = this;
    setTimeout(function(){that.loggedin()},10000);
  }
  req.onError = function(e){
    console.log(e);
  }
  req.send();
}

Login.prototype.updateGui = function(new_value) {
  if(new_value != this.isloggedin) {
    this.isloggedin = new_value;
    if (this.isloggedin==true){
      this.dispatchEvent({'type':'LOGGED_IN',value: null});
      dd.dom.removeClass(this.logoutDom,'hide');
      dd.dom.addClass(this.loginDom,'hide');
    }
    else {
      this.dispatchEvent({'type':'LOGGED_OUT', value: null});
      dd.dom.addClass(this.logoutDom,'hide');
      dd.dom.removeClass(this.loginDom,'hide');
    }
  }
}

Login.prototype.doLogout = function() {
  req = new dd.net.XhrIo('/logout','GET',null,this);
  req.onComplete = function(e) {
    this.updateGui(e.responseJson);
  }
  req.onError = function(e){
    console.log(e);
  }
  req.send();
}
Login.prototype.doKeyPress= function(e) {
  if(e.key=="Enter") {
    e.preventDefault();
    this.doLogin();
  }
}

Login.prototype.doLogin = function() {
  var u = this.username.value;
  var p = this.password.value;
  req = new dd.net.XhrIo('/login','POST', 'username=' + u + '&password=' + p,this);

  req.onComplete = function(e) {
    this.username.value = '';
    this.password.value = '';
    this.updateGui(e.responseJson);
  }
  req.onError = function(e){
    console.log(e);
    this.username.value = '';
    this.password.value = '';
  }
  req.setRequestHeader({'Content-type': 'application/x-www-form-urlencoded'});
  req.send();
}
