var Notifications;
start_app = function() {
  return new App();
}
App = function() {
  this.url = new dd.url.UrlHandler();
  this.footer = dd.dom.getElement('footer');
  this.url.listen('HASH_CHANGED', this.navigate, this);
  Notifications = new Notification();

  this.Login = new Login();

  this.Brewings = new Brewings(this);
  this.Tools = new ToolsPage();
  this.Tap = new TapPage(this);
  this.Temperatures= new Temperatures(this);
  dd.dom.append(this.Login.render(),this.footer);
  this.currentPage = this.Tap;
  var s={value: this.url.hash}
  this.hdLogo= dd.dom.getElement('hd-logo');
  dd.dom.listen(this.hdLogo,'click',function(){
    location.reload(true);
  });
  this.navigate(s);

}

App.prototype.navigate = function(s){
  switch(s.value[0]){
    case 'pa-tapp':
      this.currentPage.hide();
      this.currentPage = this.Tap;
      this.currentPage.show();
      break;
    case 'batches':

      this.currentPage.hide();
      this.currentPage = this.Brewings;
      this.currentPage.show();
      break;
    case 'temperatures':
      this.currentPage.hide();
      this.currentPage = this.Temperatures;
      this.currentPage.show();
      break;
    case 'verktyg':
      this.currentPage.hide();
      this.currentPage = this.Tools;
      this.currentPage.show();
      break;
    default:
      console.log("Okänd sida");

  }
}