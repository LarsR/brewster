


Statistic = function(data) {
  this.title = data.title;
  this.id = data.id;
  this.Draw();
}

Statistic.prototype.Mock3H = function(){

return {
  from: "2017-08-31 15:04:05",
  to: "2017-08-31 18:04:05",
  data: [
    {time:'2017-08-31 15:04:05',active:true,temp: 4.8},
    {time:'2017-08-31 16:04:05',active:false,temp: 2.5},
    {time:'2017-08-31 16:30:05',active:true,temp: 14.6},
    {time:'2017-08-31 17:04:05',active:false,temp: 7.5},
    {time:'2017-08-31 18:04:00',active:true,temp: 4.45}]
  }
};

Statistic.prototype.Draw = function() {
  this.evh = new dd.events.EventHandler();
  this.canvas = dd.dom.createElement('canvas',{class:'statistic-canvas', height: 720, width: 1280});
  this.context = this.canvas.getContext('2d');
  this.context.beginPath();
  this.closeButton = dd.dom.createElement('button',{class:'statistic-close'},'X');
  this.header = dd.dom.createElement('div',{class:'statistic-header'},[
    dd.dom.createElement('div',{class:'right'},this.closeButton),
    dd.dom.createElement('span',{class:'statistic-title'},this.title)
  ]);

  this.footer = dd.dom.createElement('div',{class:'statistic-footer'},[
    dd.dom.createElement('div',{class:'right'})
  ]);

  this.html = dd.dom.createElement('div',{class:'statistic-container'},[
    this.header,
    this.canvas,
    this.footer
  ]);
  this.cover= dd.dom.createElement('div',{class:'statistic-cover'});
  dd.dom.append(this.cover, document.body);
  dd.dom.append(this.html, document.body);
  this.evh.listenOnce(this.closeButton,'click',this.Dispose,this);
  var data = {id: this.id}

  var req = new dd.net.XhrIo("/get_statistics","POST",'id='+this.id,this);
  req.onError= function(e) {
    console.log(e);

  }
  req.onComplete = function(e){
    this.data = e.responseJson;
    this.DrawTemperatureBar();
    this.DrawTimeBar();
    this.DrawData();

  }
  req.setRequestHeader({'Content-type': 'application/x-www-form-urlencoded'});
  req.send();


}
Statistic.prototype.DrawTemperatureBar = function() {
  this.context.beginPath();
  this.context.font="20px Tahoma";
  this.context.fillText('°C', 30, 30);
  this.context.strokeStyle="#000000";
  this.context.moveTo(40, 40);
  this.context.lineTo(40, 660);
  var h = 640/23;
  var g=0;
  for(t=660-h; t >= 40;t=t-h) {
    this.context.moveTo(35, t);
    this.context.lineTo(45, t);
    if(g % 2 == 0) {
      this.context.font="16px  Tahoma"
      this.context.fillText(g+'', 14, t+6);
    }
    g++;
  }
     this.context.stroke();
}

Statistic.prototype.DrawTimeBar = function() {
  this.context.beginPath();
  this.context.moveTo(40, 660);
  this.context.lineTo(1240, 660);
  this.context.strokeStyle="#000000";

  var w = 1200/(1440/30);
  for(t = w+40; t < 1240; t = t+w) {
    this.context.moveTo(t, 655);
    this.context.lineTo(t, 665);

    // this.context.font="16px  Tahoma"
    // this.context.fillText(g+'', 5, t+6);
  }
  this.context.stroke();
}

Statistic.prototype.DrawData = function() {
  this.context.beginPath();
  var start = Date.parse(this.data.from);
  var to = Date.parse(this.data.to);
  var diff = to-start;

  dd.array.each(this.data.data,function(row, index) {
    if(index==0) {
      var h = 640 - ((640/23)*row.temp);
      this.context.moveTo(40, h);
    }else{
      var diff2 = Date.parse(row.time)-start;
      var t = (diff2/diff)*1200;
      var h = 640-((640/23)*row.temp);
      this.context.lineTo(t+40,h);
    }
  },this);
  this.context.strokeStyle="#FF0000";
  this.context.stroke();
}

Statistic.prototype.Dispose = function() {
  this.evh.removeAll();
  dd.dom.remove(this.html);
  dd.dom.remove(this.cover);
}