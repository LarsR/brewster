#!/bin/bash
cat dd.js > m.js
cat base.js >> m.js
cat strings.js >> m.js
cat array.js >> m.js
cat object.js >> m.js
cat date.js >> m.js
cat events.js >> m.js
cat dom.js >> m.js
cat net.js >> m.js
cat url.js >> m.js
cat template.js >> m.js
sed '/^$/d' m.js > merged.js
rm -rf m.js