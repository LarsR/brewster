dd = Object();dd.json = Object();
dd.extend = function (ChildClass, ParentClass) {
  ChildClass.prototype = new ParentClass();
  ChildClass.prototype.constructor = ChildClass;
}
dd.json.serialize = function(obj) {
  if(typeof JSON.stringify=='function') {
    return JSON.stringify(obj);
  }
};
dd.json.parse = function(str) {
  if(typeof JSON.parse=='function') {
    return JSON.parse(str);
  }
};
dd.string = Object();
dd.string.startsWith = function(str,pattern) {
  return (str.indexOf(pattern) == 0);
}
dd.array = Object();
dd.array.each = function(arr,fn,scope) {
  if (scope) {
    fn = fn.bind(scope)
  }
  if (!arr || arr == 'undefined') {
    return;
  }
  for (var i = 0; i < arr.length; i++) {
    fn(arr[i],i);
  }
};
dd.array.contains = function(arr, obj) {
  var i = arr.length;
  while (i--) {
    if(arr[i]==obj) {
      return true;
    }
  }
  return false;
};
dd.array.count = function(arr) {
  return arr.length;
};
dd.object = Object();
dd.object.each = function(obj,fn,opt_scope) {
  if (opt_scope) {
    fn = fn.bind(opt_scope)
  }
  for (var key in obj) {
    if(obj.hasOwnProperty(key)){
      fn(obj[key],key);
    }
  }
};
dd.object.merge = function(target,src) {
  dd.object.each(src,function(val,key) {
    target[key] = val;
  });
  return target;
};
dd.object.count = function (obj) {
var i = 0;
  for (var key in obj) {
    if(obj.hasOwnProperty(key)){
      i++;
    }
  }
  return i;
}
dd.object.findKey = function(obj,key) {
  var o =  obj[key];
  if (o) {
    return o;
  }
  else {
    return null;
  }
};
dd.date = Object();
dd.date.Now = function() {
  var datetime = new dd.date.Date();
  return datetime;
};
dd.date.Date = function() {
  this.datetime = new Date();
};
dd.date.Date.prototype.parse = function(format) {
  var month_ = (this.datetime.getMonth() < 10)?'0'+ (this.datetime.getMonth()+1):this.datetime.getMonth()+1;
  var date_ = (this.datetime.getDate() < 10)?'0'+ this.datetime.getDate():this.datetime.getDate();
  var hour_ = (this.datetime.getHours() < 10)?'0'+ this.datetime.getHours():this.datetime.getHours();
  var minute_ = (this.datetime.getMinutes() < 10)?'0'+ this.datetime.getMinutes():this.datetime.getMinutes();
  format = format.replace('YYYY',this.datetime.getFullYear());
  format = format.replace('mm',month_);
  format = format.replace('dd',date_);
  format = format.replace('HH',hour_);
  format = format.replace('ii',minute_);
  return format;
}
dd.events = Object();
dd.events.EventHandler = function() {
  this.listeners = Array();
}
dd.events.EventHandler.prototype.listen = function(obj,key,fn,scope) {
  if(scope) {
    fn = fn.bind(scope);
  }
  if(typeof key == 'object') {
    dd.array.each(key,function(k) {
      obj.addEventListener(k,fn);
      this.listeners.push({'key':k,'obj':obj,'fn':fn});
    },this);
  }
  else {
    obj.addEventListener(key,fn);
    this.listeners.push({'key':key,'obj':obj,'fn':fn});
  }
}
dd.events.EventHandler.prototype.listenOnce = function(obj,key,fn,scope) {
  if(scope) {
    fn = fn.bind(scope);
  }
  if ( typeof key == 'array') {
    dd.array.each(key,function(k){
      obj.addEventListener(k,fn);
      this.listeners.push({'key':k,'obj':obj,'fn':fn});
    },this);
  }
  else {
    obj.addEventListener(key,fn);
    this.listeners.push({'key':key,'obj':obj,'fn':fn});
  }
}
dd.events.EventHandler.prototype.removeAll = function() {
  dd.array.each(this.listeners,function(value) {
    value['obj'].removeEventListener(value['key'],value['fn']);
  });
  this.listeners = Array();
}
dd.events.listen = function(obj,key,fn,scope) {
  if(scope){
    fn = fn.bind(scope);
  }
  obj.addEventListener(key,fn);
}
dd.events.EventTarget = function() {
  this.listeners = {};
};
dd.events.EventTarget.prototype.listen = function(key,fn,scope) {
  if(Object.prototype.toString.call( key ) === '[object Array]'){
    dd.array.each(key,function(value){
      if (!this.listeners[value]) {
        this.listeners[value] = [];
      }
      this.listeners[value].push({'fn':fn,'scope':scope});
    },this);
  }
  else {
    if (!this.listeners[key]) {
      this.listeners[key] = [];
    }
    this.listeners[key].push({'fn':fn,'scope':scope});
  }
};
dd.events.EventTarget.prototype.dispatchEvent = function(data) {
  var key = "";
  if (typeof data == 'object'){
    key = data['type'];
  }
  if (this.listeners[key]) {
    this.data = data;
    dd.array.each(this.listeners[key],function(obj){
      var scope = obj['scope'];
      var func = obj['fn'].bind(scope);
      func(data);
    },this);
  }
};
dd.dom = Object();
dd.dom.getElement = function(id) {
  var el = document.getElementById(id);
  return el;
};
dd.dom.createElement = function(tag,attributes,content) {
  var dom = document.createElement(tag);
  if (typeof attributes == 'object') {
    dd.object.each(attributes,function(value,key){
      dom.setAttribute(key,value);
    });
  }
  if (content ) {
    dd.dom.append(content,dom);
  }
  return dom;
};
dd.dom.insertFirst = function(srcNode,targetNode) {
  if (targetNode.childNodes.length > 0 ) {
    targetNode.insertBefore(srcNode, targetNode.firstChild);
  }
  else {
    targetNode.appendChild(srcNode);
  }
}
dd.dom.setContent = function(srcNode,targetNode) {
  dd.dom.removeChildren(targetNode);
  if (typeof srcNode == 'string') {
    targetNode.appendChild(document.createTextNode(srcNode));
  }
  else if(Object.prototype.toString.call( srcNode ) === '[object Array]') {
    dd.array.each(srcNode,function(value){
      dd.dom.append(value,targetNode);
    });
  }
  else if (srcNode) {
    targetNode.appendChild(srcNode);
  }
  else{
    console.info("unknown type");
  }
}
dd.dom.remove = function (domNode) {
  if (domNode.parentNode) {
    domNode.parentNode.removeChild( domNode );
  }
}
dd.dom.append = function(srcNode,targetNode) {
  if (typeof srcNode == 'string') {
    targetNode.appendChild(document.createTextNode(srcNode));
  }
  else if(Object.prototype.toString.call( srcNode ) === '[object Array]') {
    dd.array.each(srcNode,function(value){
      dd.dom.append(value,targetNode);
    });
  }
  else if (srcNode) {
    targetNode.appendChild(srcNode);
  }
  else{
    console.info("unknown type");
  }
}
dd.dom.removeChildren = function(element) {
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
}
dd.dom.getParent = function(element) {
  return element.parentNode;
}
dd.dom.getClasses = function(element) {
  var classes = element.getAttribute('class').split(" ");
  return classes;
};
dd.dom.setClass = function(element,cls) {
 element.classList = cls;
};
dd.dom.swapClass = function(element,oldcls,newcls) {
 element.classList.add(newcls);
 element.classList.remove(oldcls);
};
dd.dom.addClass = function(element,cls) {
 element.classList.add(cls);
};
dd.dom.removeClass = function(element,cls) {
  element.classList.remove(cls);
};
dd.dom.hasClass = function(element,cls) {
  return element.classList.contains(cls);
};
dd.dom.toggleClass = function(element,cls) {
  if (element.classList.contains(cls)) {
    dd.dom.removeClass(element,cls);
  }else{
    dd.dom.addClass(element,cls);
  }
};
dd.dom.handleClass = function(element,cls,add) {
  if (add) {
    element.classList.add(cls);
  }
  else {
    element.classList.remove(cls);
  }
}
dd.dom.listen = function(obj,key,fn,scope) {
  if(scope){
    fn = fn.bind(scope);
  }
  obj.addEventListener(key,fn);
}
dd.net = Object();
dd.net.XhrIo = function(url,method,data,scope) {
  this.url=url;
  this.method=method;
  this.data = data;
  this.requestHeaders = {};
  this.scope = scope;
  this.evh = new dd.events.EventHandler();
  this.xhr = new XMLHttpRequest();
  this.evh.listen(this.xhr,'load',function(e) {
    if (this.xhr.status != 200) {
      if (typeof this.onError == 'function') {
        this.onError({
          status : this.xhr.status,
          text : this.xhr.statusText
        });
      }
      return;
    }
    if (typeof this.onComplete == 'function') {
      if (this.scope){
        this.onComplete= this.onComplete.bind(this.scope);
      }
      var json={};
      try {
        var json = dd.json.parse(this.xhr.responseText);
      } catch(w){};
      this.onComplete({
          status : this.xhr.status,
          text : this.xhr.statusText,
          responseJson : json,
          responseText : this.xhr.responseText,
          responseType : this.xhr.responseType
        });
    }
  },this);
  this.evh.listen(this.xhr,'error',function(e) {
    if (typeof this.onError == 'function') {
      this.onError(this.xhr);
    }
  },this);
}
dd.extend(dd.net.XhrIo,dd.events.EventTarget);
dd.net.XhrIo.prototype = {
  send : function() {
    this.xhr.open(this.method, this.url);
    dd.object.each(this.requestHeaders,function(value,key) {
      this.xhr.setRequestHeader(key,value);
    },this);
    //f = new FormData();
    // if (typeof this.data == 'object' ){
    //   dd.object.each(this.data,function(value,key){
    //     f.set(key,value);
    //   },this);
    // }else{
    //   console.log("else");
    //   f = this.data;
    // }
    this.xhr.send(this.data);
  },
  setRequestHeader : function(headers) {
    dd.object.each(headers,function(value,key){
      this.requestHeaders[key] = value;
    },this);
  }
}
dd.url = Object();
dd.url.UrlHandler = function() {
  this.protocol = document.location.protocol
  this.update();
  dd.events.listen(window,'hashchange',this.hashChanged,this);
};
dd.extend(dd.url.UrlHandler,dd.events.EventTarget);
dd.url.UrlHandler.prototype.update = function() {
  this.protocol = document.location.protocol
  this.url = this.protocol + "//" + document.location.hostname + document.location.pathname;
  this.path = document.location.pathname;
  this.host = document.location.hostname;
  this.hashString = document.location.hash;
  this.hash = Array();
  if (dd.string.startsWith(this.hashString,"#")) {
    this.hash = this.hashString.substring(1).split("/");
  }
};
dd.url.UrlHandler.prototype.setHash = function(str) {
  document.location.hash = str;
}
dd.url.UrlHandler.prototype.back = function() {
  window.history.back();
}
dd.url.UrlHandler.prototype.hashChanged = function() {
  this.update();
  this.dispatchEvent({'type':'HASH_CHANGED','value':this.hash});
};
if (!dd) {
  dd = Object();
}
dd.TemplateFromFile = function(filename,data,target) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', filename, false);
  xhr.send();
  template = xhr.responseText;
  return new dd.Template(template,data,target);
};
dd.Template = function(template,data,target) {
    this.build(template,data,target);
}
dd.Template.prototype = {
  build : function(template,data,target) {
    this.regexp = /{{([^}}]+)?}}/g;
    this.data = data;
    this.monitoredNodes = Object();
    var tmp = document.createElement('div');
    tmp.innerHTML = template;
    this.domNodes = tmp.childNodes;
    if (this.data !=null && this.data != false) {
      dd.object.each(this.data,function(val,key){
        this.monitoredNodes[key]=[];
      },this);
      Object.defineProperty(this.data, "watch", {
        enumerable : false,
        configurable : true,
        writable : false,
        value : function (handler, scope) {
          dd.object.each(this,function(value,key) {
            var oldval = value,
            newval = oldval,
            getter = function () {
              print
              return newval;
            },
            setter = function (val) {
              newval = val;
              handler.call(this, key, val, scope);
              return newval;
            };
            Object.defineProperty(this, key, {
               get: getter,
               set: setter,
               enumerable: false,
               configurable: true
            });
          },this);
        }
      });
      dd.array.each(this.domNodes,this.walkthru,this);
      this.data.watch(this.update,this);
    }
    if (target) {
      this.render(target);
    }
  },
  update : function(key,newValue,scope) {
    dd.array.each(scope.monitoredNodes[key],function(item) {
      var node = item['target'];
      var orginal = item['orginal'];
      var newstr=orginal;
      while(match = scope.regexp.exec(orginal)) {
        var newData = "";
        //if (scope.data[match[1]]) {
          newData = scope.data[match[1]];
        //}
        newstr = newstr.replace(match[0],newData);
      }
      if (node.nodeType == 2) {
        node.value = newstr;
      }
      else if (node.nodeType == 3) {
        node.nodeValue = newstr;
      }
    });
  },
  parse : function(node) {
    if (node.nodeType == 1) {
      dd.object.each(node.attributes,function(attribute) {
        var orginal = attribute.value;
        while (match = this.regexp.exec(attribute.value)) {
          var newData = "";
          //if (this.data[match[1]]) {
            newData = this.data[match[1]];
          //}
          attribute.value = attribute.value.replace(match[0],newData);
          this.monitoredNodes[match[1]].push({'orginal':orginal,'target':attribute});
        }
      },this);
    }
    else if (node.nodeType == 3) {
      var orginal = node.nodeValue;
      while(match = this.regexp.exec(node.nodeValue)) {
        var newData = "";
        if (this.data[match[1]]) {
          newData = this.data[match[1]];
        }
        node.nodeValue = node.nodeValue.replace(match[0],newData);
        this.monitoredNodes[match[1]].push({'orginal':orginal,'target':node});
      }
   }
  },
  walkthru : function(node,index) {
    this.parse(node);
    if (node.nodeType == 1 && node.childNodes.length > 0) {
      dd.array.each(node.childNodes,this.walkthru,this);
    }
  },
  render : function(target) {
    var a = [];
    while(this.domNodes.length) {
      a.push(target.appendChild(this.domNodes[0]));
    }
    this.domNodes=a;
  },
  remove : function() {
    if(!this.domNodes) return;
    for (var i = 0; i < this.domNodes.length; i++) {
      this.domNodes[i].parentElement.removeChild(this.domNodes[i]);
    }
      this.data = null;
      this.monitoredNodes = null;
      this.domNodes = null;
  },
  nodeByKey : function(key) {
    var nodes = Array();
    if (!this.monitoredNodes[key]) {
      return null;
    }
    dd.object.each(this.monitoredNodes[key],function(node) {
      switch (node['target'].nodeType) {
        case 2:
          nodes.push(node['target'].ownerElement);
          break;
        case 3:
          nodes.push(node['target'].parentNode);
          break;
      }
    },this);
    if (nodes.length > 0 ){
      return nodes[0];
    }
    return null;
  }
};
