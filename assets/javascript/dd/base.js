dd.json = Object();

dd.extend = function (ChildClass, ParentClass) {
  ChildClass.prototype = new ParentClass();
  ChildClass.prototype.constructor = ChildClass;
}


dd.json.serialize = function(obj) {
  if(typeof JSON.stringify=='function') {
    return JSON.stringify(obj);
  }
};



dd.json.parse = function(str) {
  if(typeof JSON.parse=='function') {
    return JSON.parse(str);
  }
};

