dd.net = Object();

dd.net.XhrIo = function(url,method,data,scope) {
  this.url=url;
  this.method=method;
  this.data = data;
  this.requestHeaders = {};
  this.scope = scope;
  this.evh = new dd.events.EventHandler();
  this.xhr = new XMLHttpRequest();

  this.evh.listen(this.xhr,'load',function(e) {
    if (this.xhr.status != 200) {
      if (typeof this.onError == 'function') {
        this.onError({
          status : this.xhr.status,
          text : this.xhr.statusText
        });
      }
      return;
    }
    if (typeof this.onComplete == 'function') {
      if (this.scope){
        this.onComplete= this.onComplete.bind(this.scope);
      }
      var json={};
      try {
        var json = dd.json.parse(this.xhr.responseText);
      } catch(w){};
      this.onComplete({
          status : this.xhr.status,
          text : this.xhr.statusText,
          responseJson : json,
          responseText : this.xhr.responseText,
          responseType : this.xhr.responseType
        });
    }
  },this);

  this.evh.listen(this.xhr,'error',function(e) {
    if (typeof this.onError == 'function') {
      this.onError(this.xhr);
    }
  },this);
}
dd.extend(dd.net.XhrIo,dd.events.EventTarget);

dd.net.XhrIo.prototype = {
  send : function() {
    this.xhr.open(this.method, this.url);
    dd.object.each(this.requestHeaders,function(value,key) {
      this.xhr.setRequestHeader(key,value);

    },this);

    //f = new FormData();
    // if (typeof this.data == 'object' ){
    //   dd.object.each(this.data,function(value,key){
    //     f.set(key,value);
    //   },this);
    // }else{
    //   console.log("else");
    //   f = this.data;
    // }
    this.xhr.send(this.data);
  },

  setRequestHeader : function(headers) {
    dd.object.each(headers,function(value,key){
      this.requestHeaders[key] = value;
    },this);
  }
}
