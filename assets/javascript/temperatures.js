Temperatures = function(app) {
  this.app = app;
  this.focus = false;
  this.thermostats =[
  new Thermostat({title: "Första Jäskyl", name: 'jaskyl1',targetTemperature: 12.0,id: 0},this),
  new Thermostat({title: "Lagrings-kylskåp", name: 'fatkyl',targetTemperature: 3.0,id: 1},this),
  new Thermostat({title: "Rums-temperatur", name: 'room',targetTemperature: 18.0,id: 2},this)
  ];
  this.render();

}

Temperatures.prototype.show = function() {
  this.focus = true;
  dd.dom.removeClass(this.html,'hide');
  this.update();
}

Temperatures.prototype.hide = function() {
  this.focus = false;
  dd.dom.addClass(this.html,'hide');
}

Temperatures.prototype.update = function() {

  var req = new dd.net.XhrIo("/sensors","GET",null,this);
  req.onComplete = function(d) {
    dd.array.each(d.responseJson,function(d) {
      if (!this.thermostats[d.id].lock) {
        this.thermostats[d.id].update(d);
      }
    }, this)
  }
  req.onError = function(e){
    console.log(e);
  }
  req.send();
  if(this.focus){
    var that = this;
    setTimeout(function(){that.update();},1500);
  }
}

Temperatures.prototype.render = function() {
  this.html = dd.dom.createElement('div',{class: 'thermostats hide'});
  dd.dom.append(this.html, dd.dom.getElement('main'));
  dd.array.each(this.thermostats,function(f){ dd.dom.append(f.render(),this.html)},this);
}



Thermostat = function(obj,owner) {
  this.owner = owner;
  this.id = obj.id;
  this.title = obj.title;
  this.name = obj.name;
  this.targetTemperature = 12.0;
  this.currentTemperature = 12.0;
  this.active = false;
  this.on = false;
  this.cool = true;
  this.evh = new dd.events.EventHandler();
  this.lock = false;
}

Thermostat.prototype.update = function(data) {
  if(dd.dom.hasClass(this.domBtnEdit,'hide') && this.owner.app.Login.isloggedin){
    dd.dom.removeClass(this.domBtnEdit,'hide');
  }
  if(!dd.dom.hasClass(this.domBtnEdit,'hide') && !this.owner.app.Login.isloggedin){
    dd.dom.addClass(this.domBtnEdit,'hide');
  }
  if(data.cool != this.cool) {
    this.cool = data.cool;
    if (!this.cool) {
      dd.dom.swapClass(this.domStatus,'cooling','heating');
      dd.dom.swapClass(this.adminHeatBtn,'cool','heat');
    }else{
      dd.dom.swapClass(this.domStatus,'heating','cooling');
      dd.dom.swapClass(this.adminHeatBtn,'heat','cool');
    }
  }

  if(data.active != this.active) {

    this.active = data.active;
    if (this.active) {
      dd.dom.addClass(this.domStatus,'active');
    }else{
      dd.dom.removeClass(this.domStatus,'active');
    }
  }

  if(data.on != this.on) {
    this.on = data.on;
    if(this.on){
      dd.dom.removeClass(this.html,'inactive');
      dd.dom.swapClass(this.adminOnBtn,'off','on');

    }else {
      dd.dom.addClass(this.html,'inactive');
      dd.dom.swapClass(this.adminOnBtn,'on','off');
    }
  }
  this.currentTemperature = data.currentTemperature

  dd.dom.setContent(this.currentTemperature + "°C", this.domCurrentTemperature);

  if (this.domTargetTemperature !== document.activeElement ) {
    this.targetTemperature = data.targetTemperature;
    this.adminTargetTemperature.value=this.targetTemperature;
    dd.dom.setContent(this.targetTemperature + "°C", this.domTargetTemperature);
  }
}

Thermostat.prototype.set = function(data){
  var req = new dd.net.XhrIo("/set_thermostat","POST",JSON.stringify(data),this);
  req.onError= function(e){
    console.log(e);
  }
  req.onComplete = function(e){
    var d = e.responseJson;
    this.update(d);
  }
  req.setRequestHeader({'Content-Type' : 'application/json; charset=UTF-8'})
  req.send();

}
Thermostat.prototype.toggleAdminView = function(e) {
  if(this.adminView.offsetHeight > 0){    
    this.lock = false;
    dd.dom.swapClass(this.adminView,'show','hide');
  } else {
    this.lock = true;
    dd.dom.swapClass(this.adminView,'hide','show');
  }
  var r = document.body.offsetWidth - this.domBtnEdit.offsetLeft;
  var b = window.innerHeight - (this.html.offsetHeight + this.domBtnEdit.offsetTop+this.domBtnEdit.offsetHeight);
  this.adminView.style.right = r + "px";
  this.adminView.style.bottom = b + "px";
}

Thermostat.prototype.toogleOnOff = function(){
  if (this.on) {
    dd.dom.swapClass(this.adminOnBtn,'on','off');

  }else{
    dd.dom.swapClass(this.adminOnBtn,'off','on');

  }
  var data = {cool: this.cool, on: !this.on, id: this.id,targetTemperature: this.targetTemperature}
  this.set(data);
}

Thermostat.prototype.toogleHeatCool = function(){
if (this.cool) {
    dd.dom.swapClass(this.adminHeatBtn,'cool','heat');

  }else{
    dd.dom.swapClass(this.adminHeatBtn,'heat','cool');
  }
  var data = {cool: !this.cool, on: this.on, id: this.id,targetTemperature: this.targetTemperature}
  this.set(data);
}

Thermostat.prototype.setTargetTemperature = function(){
  this.targetTemperature = this.adminTargetTemperature.value;
  var data = {cool: this.cool, on: this.on, id: this.id,targetTemperature: parseFloat(this.targetTemperature)}
  this.set(data);
}

Thermostat.prototype.showGraph = function() {
new Statistic({title: this.title,id: this.id});
}

Thermostat.prototype.render = function() {

  this.domStatus = dd.dom.createElement('div',{class:'thermostat-status cooling'});
  this.domCurrentTemperature = dd.dom.createElement('span',{class:'thermostat-current-temperature'},"-127°C");
  this.domTargetTemperature = dd.dom.createElement('div',{class: 'thermostat-target-temperature'},'0°C');

  this.domGraphBtn = dd.dom.createElement('button',{class:'thermostat-graph'});
  this.domBtnEdit = dd.dom.createElement('button',{class:'thermostat-edit-btn hide'});
  this.adminHeatBtn = dd.dom.createElement('button',{class: 'heat-cool-button cool'});
  this.adminOnBtn = dd.dom.createElement('button',{class: 'on-off-button off'});

  this.adminTargetTemperature = dd.dom.createElement('input',{type:'number',step: 0.5, class: 'admin-target-temperature',value: this.targetTemperature});
    this.evh.listen(this.domBtnEdit,'click',this.toggleAdminView,this);
    this.evh.listen(this.adminOnBtn,'click',this.toogleOnOff,this);
    this.evh.listen(this.domGraphBtn,'click',this.showGraph,this);
    this.evh.listen(this.adminHeatBtn,'click',this.toogleHeatCool,this);
    this.evh.listen(this.adminTargetTemperature,'change',this.setTargetTemperature,this);
  if (this.owner.app.Login.isloggedin){
    dd.dom.removeClass(this.domBtnEdit,hide);
  }

  this.adminView = dd.dom.createElement('div',{class:'thermostat-admin hide'},[
    dd.dom.createElement('div',{class: 'admin-row'},[
      dd.dom.createElement('span',{class:'admin-row-title'},"On/Off:"),
      this.adminOnBtn
    ]),
    dd.dom.createElement('div',{class: 'admin-row'},[
      dd.dom.createElement('span',{class:'admin-row-title'},"Cool/Heat:"),
      this.adminHeatBtn
    ]),
    dd.dom.createElement('div',{class: 'admin-row'},[
      dd.dom.createElement('span',{class:'admin-row-title'},"Target Temperature:"),
      this.adminTargetTemperature
    ])
  ]);

  this.html = dd.dom.createElement('div',{class: 'thermostat-lcd'},[
    this.adminView,
    dd.dom.createElement('div',{class:'thermostat-header'},[
      dd.dom.createElement('div',{class:'thermostat-title'},this.title)
    ]),
    dd.dom.createElement('div',{class: 'thermostat-subtitle'},"Target / Current"),
    dd.dom.createElement('div',{class: 'thermostat-mid'},[
      this.domTargetTemperature,
      " / ",
      this.domCurrentTemperature
    ]),
    dd.dom.createElement('div',{class: 'thermostat-footer'},[
      this.domStatus,
      this.domGraphBtn,
      this.domBtnEdit
    ])
  ]);

  if (!this.cool) {
    dd.dom.setClass(this.domStatus,'thermostat-status heating');
    dd.dom.swapClass(this.adminHeatBtn,'cool','heat');
  }

  if (!this.on) {
    dd.dom.addClass(this.html,'inactive');
    dd.dom.swapClass(this.adminOnBtn,'on','off');
  }

  return this.html;
}
