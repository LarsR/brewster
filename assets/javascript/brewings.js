Brewings = function(owner) {
  this.items=[];
  this.owner = owner;
  this.evh = new dd.events.EventHandler();
  this.render();
  this.load_brewings();
}

Brewings.prototype.show = function() {
  dd.dom.removeClass(this.html,'hide');
}

Brewings.prototype.hide = function() {
  dd.dom.addClass(this.html,'hide');
}

Brewings.prototype.update = function() {

}

Brewings.prototype.render = function() {
  this.html = dd.dom.createElement('div',{class:'brewings hide'});
  this.table= dd.dom.createElement('table',{width:'100%'},[
    dd.dom.createElement('tr',{},[
      dd.dom.createElement('td',{},'Bryggdag'),
      dd.dom.createElement('td',{},'Namn'),
      dd.dom.createElement('td',{},'Beskrivning'),
      dd.dom.createElement('td',{},'Ingredienser'),
      dd.dom.createElement('td',{},'Anteckningar'),
      dd.dom.createElement('td',{},'Volym %'),
      dd.dom.createElement('td',{},'')
    ])
  ]);
  this.newBrewing = dd.dom.createElement('button',{'class':'add-btn hide'},'');
  this.evh.listen(this.newBrewing,'click',this.addBrewing,this);

  this.menu = dd.dom.createElement('div',{'class':'sub-menu'},[
    this.newBrewing
  ]);

  dd.dom.append(this.menu, this.html);
  this.owner.Login.listen('LOGGED_OUT',this.onLogout,this);
  this.owner.Login.listen('LOGGED_IN',this.onLogin,this);
  dd.dom.append(this.table, this.html);
  dd.dom.append(this.html, dd.dom.getElement('main'));
}


Brewings.prototype.addBrewing = function() {
  var num =this.items.length+1;
  var b = this.add({
            uid: 0,
            name: 'Bryggning #'+ num,
            ingredients:'',
            description: '',
            scheme: '',
            og: 1.052,
            fg: 1.012,
            ibu: 20,
            tap: 0
          });
  new Brewing.Edit(b)
}

Brewings.prototype.onLogin = function(){
  dd.dom.removeClass(this.newBrewing,'hide');
}

Brewings.prototype.onLogout = function(){

  dd.dom.addClass(this.newBrewing,'hide');
}



Brewings.prototype.buildHtml = function(){
  items = [];
  dd.array.each(this.items,function(b){
    items.push(b.html);
  },this);
  return items;
}


Brewings.prototype.load_brewings = function(){
  req = new dd.net.XhrIo('/brewings','GET', null,this);
  req.onComplete = function(d){
    dd.array.each(d.responseJson,this.add,this)
  }
  req.onError = function(e){
    console.log(e);
  }
  req.send();
}

Brewings.prototype.add = function(data) {
  var brew = new Brewing(data, this)
  this.items.push(brew);
  return brew;
}

Brewings.prototype.getById = function(id) {
  var b = null;
  dd.array.each(this.items,function(brewing){
    if (brewing.uid==id){
      b=brewing;
      return
    }
  },this);
  return b
};

Brewings.prototype.render_no_brewings = function() {
  this.html = dd.dom.createElement('div',{class:'hide'},"Det finns inga sparade Bryggningar");
  dd.dom.append(this.html, dd.dom.getElement('main'));

}

Brewings.prototype.pause = function() {

}

