Brewing = function(data, owner) {
  this.uid          = data.uid;
  this.name         = data.name;
  this.ingredients  = data.ingredients;
  this.description  = data.description;
  this.scheme       = data.scheme;
  this.fg           = data.fg;
  this.og           = data.og;
  this.ibu          = data.ibu;
  this.brewingday   = data.brewingday;
  this.app = owner.owner;
  this.render(owner.table);
}

Brewing.prototype.render = function(target) {
  this.domDescription = dd.dom.createElement('div',{class: 'full one-line'});
  this.domIngredients = dd.dom.createElement('div',{class: 'full one-line'});
  this.domScheme      = dd.dom.createElement('div',{class: 'full one-line'});
  this.domName = dd.dom.createElement('td',{});
  this.domBrewday = dd.dom.createElement('td',{});
  this.domAlcohol = dd.dom.createElement('td',{});
  this.domEditBtn = dd.dom.createElement('button',{class:'edit hide'});
  this.domToggleExpandBtn = dd.dom.createElement('button',{class: 'toggle up'});
  this.html = dd.dom.createElement('tr',{class:'brewing'},[
      this.domBrewday,
      this.domName,
      dd.dom.createElement('td',{},this.domDescription),
      dd.dom.createElement('td',{},this.domIngredients),
      dd.dom.createElement('td',{},this.domScheme),
      this.domAlcohol,
      dd.dom.createElement('td',{},[
        this.domToggleExpandBtn,
        this.domEditBtn
      ])
  ]);

  this.evh = new dd.events.EventHandler();
  this.app.Login.listen('LOGGED_OUT',this.onLogout,this);
  this.app.Login.listen('LOGGED_IN',this.onLogin,this);

  this.evh.listen(this.domEditBtn,'click',function(){new Brewing.Edit(this);}, this);


  this.evh.listen(this.domToggleExpandBtn,'click',function(){
    dd.dom.toggleClass(this.domIngredients, 'one-line');
    dd.dom.toggleClass(this.domDescription, 'one-line');
    dd.dom.toggleClass(this.domScheme, 'one-line');
    dd.dom.toggleClass(this.domToggleExpandBtn, 'up');
    dd.dom.toggleClass(this.domToggleExpandBtn, 'down');
  }, this);



  if(this.app.Login.isloggedin){
    this.onLogin();
  }
  dd.dom.append(this.html, target);
  this.redraw();
}

Brewing.prototype.redraw = function() {
  dd.dom.removeChildren(this.domDescription);
  dd.dom.removeChildren(this.domIngredients);
  dd.dom.removeChildren(this.domScheme);
  this.domDescription.insertAdjacentHTML('beforeend',this.description);
  this.domIngredients.insertAdjacentHTML('beforeend',this.ingredients);
  this.domScheme.insertAdjacentHTML('beforeend',this.scheme);
  dd.dom.setContent(this.name,this.domName);
  dd.dom.setContent(this.brewingday,this.domBrewday);

  this.alcohol = Tools.CalculateAlcohol(this.og,this.fg);
  dd.dom.setContent(this.alcohol+"%",this.domAlcohol);
}

Brewing.prototype.onLogin = function(){
  dd.dom.removeClass(this.domEditBtn,'hide');
}

Brewing.prototype.onLogout = function(){
  dd.dom.addClass(this.domEditBtn,'hide');
}

Brewing.prototype.save = function() {
  var data = {
    uid: this.uid,
    name: this.name,
    description:  this.description,
    scheme:  this.scheme,
    ingredients:  this.ingredients,
    brewingday: this.brewingday,
    og:           parseFloat(this.og),
    fg:           parseFloat(this.fg),
    ibu:          parseFloat(this.ibu)
  }
  var req = new dd.net.XhrIo("/save_brewing","POST",JSON.stringify(data),this);
  req.onError= function(e){
    Notifications.Varning(e.responseText);
  }
  req.onComplete = function(e){
    this.uid = e.responseJson.uid;
  }
  req.setRequestHeader({'Content-Type' : 'application/json; charset=UTF-8'})
  req.send();
}


Brewing.Edit = function(brewing) {
  this.evh = new dd.events.EventHandler();
  this.brewing = brewing;
  this.domBrewday = dd.dom.createElement('input',{type: 'date',value: this.brewing.brewingday})
  this.domName = dd.dom.createElement('input',{type: 'text',value: this.brewing.name})

  this.domDescription = dd.dom.createElement('div',{contentEditable:'true',class: 'editable'});
  this.domDescription.insertAdjacentHTML('beforeend',this.brewing.description);

  this.domIngredients = dd.dom.createElement('div',{contentEditable:'true',class: 'editable'});
  this.domIngredients.insertAdjacentHTML('beforeend',this.brewing.ingredients);

  this.domNotes = dd.dom.createElement('div',{contentEditable:'true',class: 'editable'});
  this.domNotes.insertAdjacentHTML('beforeend',this.brewing.scheme);

  this.domOG= dd.dom.createElement('input',{type:'number', step:'0.01', value: this.brewing.og})
  this.domFG= dd.dom.createElement('input',{type:'number', step:'0.01', value: this.brewing.fg})
  this.domIBU= dd.dom.createElement('input',{type:'number', step:'1', value: this.brewing.ibu})

  this.domSave=dd.dom.createElement('button',{class: 'text-save'},'Spara');
  this.domCancel = dd.dom.createElement('button',{class: 'text-cancel'},'Avbryt');

  this.evh.listen(this.domCancel,'click',this.cancel,this);
  this.evh.listen(this.domSave, 'click',this.save,this);

  this.cover = dd.dom.createElement('div',{class: 'cover'},[
    dd.dom.createElement('div',{class: 'tbl'},[
      dd.dom.createElement('div',{class:'tbl-row'},[
        dd.dom.createElement('div',{class:'tbl-col right-align'},'Bryggdag: '),
        dd.dom.createElement('div',{class:'tbl-col'},this.domBrewday),
        dd.dom.createElement('div',{class:'tbl-col right-align'},'Namn: '),
        dd.dom.createElement('div',{class:'tbl-col'},this.domName)
      ]),
      dd.dom.createElement('div',{class:'tbl-row'},[
        dd.dom.createElement('div',{class:'tbl-col right-align'},'Beskrivning: '),
        dd.dom.createElement('div',{class:'tbl-col'},this.domDescription),
        dd.dom.createElement('div',{class:'tbl-col right-align'},'OG: '),
        dd.dom.createElement('div',{class:'tbl-col'},this.domOG)
      ]),
      dd.dom.createElement('div',{class:'tbl-row'},[
        dd.dom.createElement('div',{class:'tbl-col right-align'},'Ingredienser: '),
        dd.dom.createElement('div',{class:'tbl-col'},this.domIngredients),
        dd.dom.createElement('div',{class:'tbl-col right-align'},'FG: '),
        dd.dom.createElement('div',{class:'tbl-col'},this.domFG)
      ]),
      dd.dom.createElement('div',{class:'tbl-row'},[
        dd.dom.createElement('div',{class:'tbl-col right-align'},'Anteckningar: '),
        dd.dom.createElement('div',{class:'tbl-col'},this.domNotes),
        dd.dom.createElement('div',{class:'tbl-col right-align'},'IBU: '),
        dd.dom.createElement('div',{class:'tbl-col'},this.domIBU)
      ])
    ]),
    dd.dom.createElement('div',{class: 'tbl'},[
      dd.dom.createElement('div',{class:'tbl-row'},[
        dd.dom.createElement('div',{class:'tbl-col right-align'},[
          this.domCancel,
          this.domSave
        ]),
      ])
    ])
  ]);
  dd.dom.append(this.cover,document.body);

}

Brewing.Edit.prototype.cancel=function(){
  this.dispose();
}

Brewing.Edit.prototype.save = function(){
  this.brewing.brewingday = this.domBrewday.value;
  this.brewing.name = this.domName.value;
  this.brewing.description = this.domDescription.innerHTML;
  this.brewing.ingredients = this.domIngredients.innerHTML;
  this.brewing.scheme = this.domNotes.innerHTML;
  this.brewing.og = this.domOG.value;
  this.brewing.fg = this.domFG.value;
  this.brewing.ibu = this.domIBU.value;
  this.brewing.save();
  this.brewing.redraw();
  this.dispose();
}
Brewing.Edit.prototype.render=function(){

}

Brewing.Edit.prototype.dispose = function() {
  this.evh.removeAll();
  dd.dom.remove(this.cover);
}