package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
	"time"
)

type Statistics struct {
	From string      `json:"from"`
	To   string      `json:"to"`
	Data []Statistic `json:"data"`
}
type Statistic struct {
	Time   string  `json:"time"`
	Active bool    `json:"active"`
	Temp   float64 `json:"temp"`
}

type DB struct {
	Connection *sql.DB
}

func (db *DB) connect() error {
	user := config.Mysql.User
	password := config.Mysql.Password
	dbname := config.Mysql.Database
	var err error
	db.Connection, err = sql.Open("mysql", user+":"+password+"@/"+dbname)
	if err != nil {
		return err
	}
	err = db.Connection.Ping()
	if err != nil {
		return err
	}

	return nil
}

func (db *DB) Destroy() error {
	err := db.connect()
	defer db.Connection.Close()
	if err != nil {
		return err
	}
	sql := `
  DROP TABLE brewings;
  `
	_, err = db.Connection.Query(sql)

	if err != nil {
		return err
	}
	return nil
}

func (db *DB) UpdateBrewing(b *Brewing) error {
	err := db.connect()
	defer db.Connection.Close()
	if err != nil {
		return err
	}
	stmtIns, err := db.Connection.Prepare("UPDATE brewings SET name=?, description=?, scheme=?, ingredients=?, og=?, fg=?, ibu=?, updated=?, brewingday=? WHERE uid=?")
	if err != nil {
		return err
	}
	defer stmtIns.Close() // Close the statement when we leave main() / the program terminates
	dt := time.Now().Format(DATE_FORMAT)
	_, err = stmtIns.Exec(b.Name, b.Description, b.Scheme, b.Ingredients, b.OG, b.FG, b.IBU, dt, b.BrewingDay, b.Uid)
	return err
}

func (db *DB) getBrewings() (Brewings, error) {
	bs := Brewings{}
	err := db.connect()
	defer db.Connection.Close()
	if err != nil {
		return bs, err
	}

	rows, err := db.Connection.Query("SELECT * FROM brewings")
	if err != nil {
		return bs, err
	}
	for rows.Next() {
		var b Brewing
		var d, u string
		err = rows.Scan(&b.Uid, &b.Name, &b.Description, &b.Scheme, &b.Ingredients, &b.OG, &b.FG, &b.IBU, &d, &u, &b.BrewingDay)
		t, _ := time.Parse(DATE_FORMAT, d)
		t2, _ := time.Parse(DATE_FORMAT, u)
		b.Created = t
		b.Updated = t2
		bs.Add(&b)
	}
	return bs, err
}

func (db *DB) GetStatistics(id int, from, to time.Time) (Statistics, error) {
	var st Statistics
	err := db.connect()
	defer db.Connection.Close()
	if err != nil {
		return st, err
	}
	st.From = from.Format(DATE_FORMAT)
	st.To = to.Format(DATE_FORMAT)

	q := "SELECT time, active, temperature FROM statistics WHERE id=" + strconv.Itoa(id) + " AND time between '" + from.Format(DATE_FORMAT) + "' AND '" + to.Format(DATE_FORMAT) + "'"
	rows, err := db.Connection.Query(q)
	if err != nil {
		return st, err
	}
	for rows.Next() {
		var s Statistic
		var a int
		err = rows.Scan(&s.Time, &a, &s.Temp)
		if err != nil {
			return st, err
		}
		if a == 1 {
			s.Active = true
		}
		st.Data = append(st.Data, s)
	}
	return st, err
}

func (db *DB) LogStatistics(id int, active bool, temp float64) error {
	dt := time.Now().Format("2006-01-02 15:04:05")
	act := 0
	if active == true {
		act = 1
	}

	err := db.connect()
	defer db.Connection.Close()
	if err != nil {
		return err
	}

	stmtIns, err := db.Connection.Prepare("INSERT INTO statistics SET id=?, time=?, active=?, temperature=?")
	if err != nil {
		return err
	}
	defer stmtIns.Close()
	_, err = stmtIns.Exec(id, dt, act, temp)
	return err
}

func (db *DB) AddBrewing(b *Brewing) error {
	err := db.connect()
	defer db.Connection.Close()
	if err != nil {
		return err
	}

	stmtIns, err := db.Connection.Prepare("INSERT INTO brewings SET name=?, description=? ,scheme=?, ingredients=?, og=?, fg=?, ibu=?, created=?, updated=?, brewingday=?")
	if err != nil {
		return err
	}
	defer stmtIns.Close() // Close the statement when we leave main() / the program terminates
	dt := time.Now().Format("2006-01-02 15:04:05")
	res, err := stmtIns.Exec(b.Name, b.Description, b.Scheme, b.Ingredients, b.OG, b.FG, b.IBU, dt, dt, b.BrewingDay)
	i, err := res.LastInsertId()
	if err != nil {
		return err
	}
	b.Uid = int(i)
	return err
}

func (db *DB) Migrate(file string) error {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		log.Print(err)
	}
	err = db.connect()
	defer db.Connection.Close()
	if err != nil {
		return err
	}
	sqls := strings.Split(string(data), ";")
	for _, sql := range sqls {
		if sql != "" {

			_, err := db.Connection.Query(sql)
			if err != nil {
				log.Print(err)
			}
		}
	}
	return err
}
